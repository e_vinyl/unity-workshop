﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]	//forces the GameObject to have BoxCOllider2D
//Class used to code the game's paddle. It has the player input
public class Paddle : MonoBehaviour {
	public float acceleration; //Paddle's acceleration in u/s^2
	public float breakingAcceleration; //Paddle's breaking acceleration in u/s^2
	public float maxSpeed = 4;	//Paddle's max speed in u/s

	private int wallLayer;	//The wall mask cache
	private BoxCollider2D boxCollider;
	private float halfXColliderSize;
	private float velocity;
	private float sense;

	// Use this for initialization
	void Start () {
		wallLayer = 1 << LayerMask.NameToLayer("Wall");	//cache's the layermask. They layer mask is creating by bitshifting 1 the number of times given by LayerMask.NameToLayer("Wall")
		boxCollider = GetComponent<BoxCollider2D>();

		halfXColliderSize = boxCollider.size.x / 2;	//Caches the paddle's collider half width
	}
	
	// Update is called once per frame
	void Update () {
		//Code to Quit
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			Application.Quit();
		}

		float axis = Input.GetAxis("Horizontal");   //Gets the value of the Horizontal Axis according to the keys/buttons defined on the Input manager

		if (axis != 0)	//If the player is moving
		{
			velocity = Mathf.Clamp(velocity + Mathf.Sign(axis) * acceleration * Time.deltaTime, -maxSpeed, maxSpeed); //Increments speed according to acceleration and clamp it according to the max speed
		}
		else if(velocity != 0)	//Does not break if paddle is already stationary
		{
			float breakingSpeed = breakingAcceleration * Time.deltaTime;    //Calculates the breaking speed from the breaking acceleration
			velocity -= sense * breakingSpeed; //Applies an acceleration contrary to the paddle's movement sense

			//Truncates the velocity when its magnitude is close to zero
			if (Mathf.Abs(velocity) <= breakingSpeed)
			{
				velocity = 0;
			}
		}
		sense = Mathf.Sign(velocity);  //gets the paddle's sense
		float displacement = velocity * Time.deltaTime;  //Calculates the final speed

		Vector2 rayOrigin = transform.position; //Caches the origin of the ray used for the raycast

		if(velocity != 0)	//Check if the paddle will collide when it is moving
		{
			//Updates the ray's origin according to the direction of the movement
			//This only works because the walls are stationary and the paddle is confined inside them
			if (sense > 0)
			{
				rayOrigin.x += boxCollider.offset.x + halfXColliderSize;    //Going right
			}
			else
			{
				rayOrigin.x += boxCollider.offset.x - halfXColliderSize;    //Going left
			}

			//Checks for walls either to the left, or to the right, of the paddle
			//Does not update movement on hits
			if (!Physics2D.Raycast(rayOrigin, Vector3.right, displacement, wallLayer))
			{
				transform.position += Vector3.right * displacement; //Increment's the paddle's displacement
			}
		}

	}
}
