﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]	//Forces GameObject to have an Animator
[RequireComponent(typeof(BoxCollider2D))]	//Forces GameObject to have a BoxCollider2D

//Class used to represent a brick
public class Brick : MonoBehaviour {
	public int score = 120;	//The points this particular brick can give

	private Animator animator;
	private BoxCollider2D boxCollider;
	private GameMaster gameMaster;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator>();
		boxCollider = GetComponent<BoxCollider2D>();
	}

	//Function used to register the GameMaster with this brick
	public void RegisterGameMaster(GameMaster master)
	{
		gameMaster = master;
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		boxCollider.enabled = false;	//disables the collider so it won't continue to block the ball
		animator.SetTrigger("Break");	//Plays an animation when brick is hit
	}

	private void DisableBrick()
	{
		if(gameMaster != null)
		{
			gameMaster.OnBrickDestroyed(score);	//Notifies the gamemaster that this brick was busted
		}
		
		gameObject.SetActive(false);	//Disables the GameObject so it disappears
	}
}
