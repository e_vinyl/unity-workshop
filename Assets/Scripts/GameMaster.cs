﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(BoxCollider2D))]
//Class used to contain the game loop logic
public class GameMaster : MonoBehaviour {
	public int lives = 3;	//Starting lives
	public Ball ballPrefab;	//Ball prefab to spawn it
	public Paddle paddle;	//Reference to the paddle
	public Transform ballSpawner;	//Reference to the ball spawning point
	public Text livesText;
	public Text scoreText;
	public Text outcomeText;
	public Image background;
	public AudioClip winClip;	//audio clip to play after winning the game
	public AudioClip loseClip;  //audio clip to play after losing the game
	public AudioClip liveLostClip;  //audio clip to play after losing a life

	private Brick[] bricks;	//every brick in the game
	private Ball ball;	//the level's ball
	private int score = 0;	//player's current score
	private AudioSource source;
	private int destroyedBricks;	//number of bricks destroyed by the player

	// Use this for initialization
	void Start () {
		source = GetComponent<AudioSource>();
		bricks = FindObjectsOfType<Brick>();	//gets every Brick object in the game

		foreach(Brick i in bricks)
		{
			i.RegisterGameMaster(this);	//register's the gamemaster with the bricks
		}

		ball = GameObject.Instantiate<Ball>(ballPrefab, ballSpawner.position, Quaternion.identity);	//instantiates the ball
	}

	//Method used to respawn the ball
	void Respawn()
	{
		//sets the ball's position to the spawner's
		ball.transform.position = ballSpawner.position;
		ball.gameObject.SetActive(true);
		ball.ResetBall();
	}

	//Method called when a brick was destroyed
	public void OnBrickDestroyed(int scoreToAdd)
	{
		score += scoreToAdd;	//adds the brick's score

		scoreText.text = score.ToString("D9");	//sets the score text with 9 digit formating

		destroyedBricks++;	//increments the number of destroyed bricks

		//checks if the number of destroyed bricks is the same as the list of every brick
		if(destroyedBricks >= bricks.Length)
		{
			ball.gameObject.SetActive(false);	//disables the ball
			SetOutcomeText("You Win", winClip);
			source.Play();

			Invoke("RestartGame", 3f);	//calls the method in 3 seconds
		}
	}

	//Method called to restart the game
	void RestartGame()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);	//reloads the currently active scene
	}

	//Called when the ball goes through the trigger
	private void OnTriggerEnter2D(Collider2D collision)
	{
		ball.gameObject.SetActive(false);   //Disables the ball so that it stops until it is respawned
		lives--;	//decrements lives
		livesText.text = lives.ToString();	//updates the live's UI with the updated number
		score = 0;	//resets the score
		scoreText.text = score.ToString("D9");  //set's the text formated for 9 digits

		//checks if player is still alive
		if (lives > 0)
		{
			source.clip = liveLostClip;
			source.pitch = 0.2f;
			source.volume = 0.5f;
			Invoke("Respawn", 0.7f);	//calls method Respawn in 0.7s
		}
		else
		{
			SetOutcomeText("You Lose", loseClip);
			Invoke("RestartGame", 3f);  //calls the method in 3 seconds
		}

		source.Play();
	}

	//Enables the text outcome
	void SetOutcomeText(string outcome, AudioClip clip)
	{
		//resets the source's audio properties
		source.pitch = 1;
		source.volume = 1;
		source.clip = clip;

		//enables the text
		outcomeText.enabled = true;
		outcomeText.text = outcome;

		background.enabled = true;

		paddle.enabled = false;	//disables the player input
	}
}
