﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]	//Forces the gameobject to require an AudioSource
[RequireComponent(typeof(Collision2D))] //Forces the gameobject to require a Collision2D

//Class used to play a sound whenever a collision occurs
public class HitSoundPlayer : MonoBehaviour {
	private AudioSource source;

	void Start()
	{
		source = GetComponent<AudioSource>();
	}

	private void OnCollisionEnter2D(Collision2D collision)	//Special method called when two bodies collide
	{
		source.Play();	//Plays soundwhenever a collision occurs
	}
}
