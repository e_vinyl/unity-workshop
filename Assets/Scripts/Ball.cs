﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(CircleCollider2D))]

//Class used to represent a ball
public class Ball : MonoBehaviour {
	public float force = 1000;	//User defined force intensity

	private Rigidbody2D body;

	// Use this for initialization
	void Start () {
		body = GetComponent<Rigidbody2D>();
		ResetBall();
	}

	//Applies a force with a random vector
	public void ResetBall()
	{
		float x = 0;
		float y = 0;

		//Chooses a random vector
		switch (Random.Range(1, 2))
		{
			case 1:	//left upper diagonal
				x = -1;
				y = 1;
				break;
			case 2:	//right upper diagonal
				x = 1;
				y = 1;
				break;
			default:
				x = 1;
				y = 1;
				break;
		}

		body.AddForce(new Vector2(x, y) * force);	//adds the force with user defined itensity
	}
}
