﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]	//Forces GameObject to have a camera component
//Class used to adjust screen size according to the screen's resolution
public class CameraSizeAdjuster : MonoBehaviour {
	public float levelWidth;	//The maximum level width

	private Camera currentCamera;

	// Use this for initialization
	void Start () {
		currentCamera = GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
		//Calculates the new ortographic size according to the screen's aspect ratio and user defined level width
		currentCamera.orthographicSize = levelWidth * Screen.height / Screen.width * 0.5f;  //0.5f is because the orthographicSize is half the size of the vertical viewing volume.
	}
}
